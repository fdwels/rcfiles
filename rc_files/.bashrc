# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#aliases
alias vi='vim'
alias vp='vimpager'
alias ls='ls --color=auto'
alias dfs='df -hT | grep -e nvme -e Usados'
alias grep='grep -i --color=auto'
alias xi='sudo xbps-install'
alias xr='sudo xbps-remove'
alias xq='sudo xbps-query'
alias shutdown='sudo shutdown'
alias sv_status='sudo sv status /var/service/*' 

#alias neofetch='clear; neofetch --colors 13 15 13 13 15 7 \
#--ascii_colors 13 15 --ascii_distro Arch_old' 

# U could use BLAG, "OS Elbrus", Trisquel, PostMarketOS,
#	PacBSD, Refracted_Deevuan, Sparky, Arch_old, or Gentoo if u want

#alias {cp,cpg}='cpg -g'
#alias {mv,mvg}='mvg -g'

# Open programm using sudo while still having the same config. 
alias N='sudo -E nnn -cdx' #usa -cdrx si vuelves a usar copy graphic
alias VIM='sudo -E vim'

# Info what's shown first on terminal 
#PS1='[\u@\h \W]\$ '
PS1='\[$(tput bold)\]\[$(tput setaf 6)\][ \[$(tput setaf 7)\]\w \[$(tput setaf 6)\]] \[$(tput setaf 6)\]> \[$(tput sgr0)\]'
#export PS1='\[$(tput bold)\]\[$(tput setaf 11)\][ \[$(tput setaf 11)\]\w \[$(tput setaf 11)\]]$ \[$(tput setaf 11)\]> \[$(tput sgr0)\]'
# DEFAULTS
export EDITOR='vim'
export VISUAL='vimpager'
export VIMPAGER_RC=~/.vimrc
#export VIMPAGER_RC='~/.vim/vimpagerrc'
export MANPAGER='vimpager'
export PAGER='vimpager'

export QT_QPA_PLATFORMTHEME="qt5ct"

#NNN CONFIG
export NNN_COLORS='6666'
#export NNN_TRASH=1 #uses trash-cli to trash instead of delete
export NNN_OPENER=~/.config/nnn/plugins/nukemod #modified version
export NNN_PLUG='d:dragdrop;u:getplugs;s:chksum' #plugins
export LC_COLLATE='C' #set dot dir/files at the beginning

[ -n "$NNNLVL" ] && PS1="<!> $PS1" #show u r in a new shell
if [ -f ~/.config/nnn/misc/quitcd/quitcd.bash ]; then
    source ~/.config/nnn/misc/quitcd/quitcd.bash
fi #enable cd on quit
